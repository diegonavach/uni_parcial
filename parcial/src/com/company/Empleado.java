package com.company;

public class Empleado extends Persona{
    private int salario;

    public Empleado(String DNI, String nombre, String direccion, int telefono, int salario) {
        super(DNI, nombre, direccion, telefono);
        this.salario = salario;
    }

    public int getSalario() {
        return salario;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }
}
