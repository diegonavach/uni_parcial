package com.company;

import java.util.Date;

public class Horario {
    private Date fecha;
    private Date hor_entr;
    private Date hor_sal;

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getHor_entr() {
        return hor_entr;
    }

    public void setHor_entr(Date hor_entr) {
        this.hor_entr = hor_entr;
    }

    public Date getHor_sal() {
        return hor_sal;
    }

    public void setHor_sal(Date hor_sal) {
        this.hor_sal = hor_sal;
    }
}
