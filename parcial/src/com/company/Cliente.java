package com.company;

import java.util.ArrayList;

public class Cliente extends Persona{
    private ArrayList<Servicio> historial =new ArrayList<>();
    private String estado;

    public Cliente(String DNI, String nombre, String direccion, int telefono, String estado) {
        super(DNI, nombre, direccion, telefono);
        this.estado = estado;
    }

    public ArrayList<Servicio> getHistorial() {
        return historial;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
