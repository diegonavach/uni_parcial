package com.company;

import java.util.ArrayList;
import java.util.Date;

public class Medico extends Empleado{
    private String esp;
    private ArrayList<Horario> horarios_mes=new ArrayList<>();
    private int num_col_med;

    public Medico(String DNI, String nombre, String direccion, int telefono, int salario, String esp, int num_col_med) {
        super(DNI, nombre, direccion, telefono, salario);
        this.esp = esp;
        this.num_col_med = num_col_med;
    }

    public ArrayList<Horario> getHorarios_mes() {
        return horarios_mes;
    }

    public String getEsp() {
        return esp;
    }

    public void setEsp(String esp) {
        this.esp = esp;
    }

    public int getNum_col_med() {
        return num_col_med;
    }

    public void setNum_col_med(int num_col_med) {
        this.num_col_med = num_col_med;
    }

    public void setHorarios_mes(ArrayList<Horario> horarios_mes) {
        this.horarios_mes = horarios_mes;
    }

    public  void  actualizar_hor(){
        Date fecha_act= new Date();
        this.horarios_mes.forEach((i)->{
            if (i.getFecha().before(fecha_act)){
                this.horarios_mes.remove(i);
            }
        });
    }


}
