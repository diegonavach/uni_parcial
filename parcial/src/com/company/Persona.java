package com.company;

public abstract class Persona {
    // ayuda a extraer datos repetidos de clases similares
    // es abstracta pues no es util crear una persona sin especificar si es empleado o cliente
    private String DNI;
    private String nombre;
    private String direccion;
    private int telefono;

    public Persona(String DNI, String nombre, String direccion, int telefono) {
        this.DNI = DNI;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public String getDNI() {
        return DNI;
    }

    public void setDNI(String DNI) {
        this.DNI = DNI;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }
}
